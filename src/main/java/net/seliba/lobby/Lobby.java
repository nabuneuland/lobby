package net.seliba.lobby;

import net.seliba.lobby.listener.*;
import net.seliba.lobby.utils.CommandSign;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Lobby extends JavaPlugin {

    private CommandSign commandSign;

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage("[Lobby] Gestartet!");

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        Bukkit.createWorld(new WorldCreator("lobby"));

        registerCommands();
        registerListener();
        removeDaylighCycle();
        removeEntities();
        //createCommandSign();
    }

    private void registerCommands() {

    }

    private void registerListener() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        PlayerJoinListener playerJoinListener = new PlayerJoinListener();
        this.commandSign = new CommandSign();

        //Register listener
        pluginManager.registerEvents(new BlockEditListener(), this);
        pluginManager.registerEvents(new EntityDamageListener(), this);
        pluginManager.registerEvents(new EntitySpawnListener(), this);
        pluginManager.registerEvents(new InventoryClickListener(this, playerJoinListener), this);
        pluginManager.registerEvents(new PlayerDoubleJumpListener(), this);
        pluginManager.registerEvents(new PlayerDropItemListener(), this);
        pluginManager.registerEvents(new PlayerInteractListener(), this);
        pluginManager.registerEvents(new PlayerHungerChangeListener(), this);
        pluginManager.registerEvents(playerJoinListener, this);
        pluginManager.registerEvents(new PlayerQuitListener(), this);
        pluginManager.registerEvents(new WeatherChangeListener(), this);
        pluginManager.registerEvents(commandSign, this);
    }

    private void removeDaylighCycle() {
        Bukkit.getWorld("lobby").setTime(1000L);
        Bukkit.getWorld("lobby").setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
    }

    private void removeEntities() {
        Bukkit.getWorld("lobby").getEntities().forEach(entity -> {
            if(entity.getType() != EntityType.PLAYER && entity.getType() != EntityType.ARMOR_STAND) {
                entity.remove();
            }
        });
    }

    private void createCommandSign() {
        Sign sign = commandSign.createWallSign(new Location(Bukkit.getWorld("lobby"), 8D, 37D, -56D),
                (player, location) -> {

                }
        );
        org.bukkit.material.Sign materialSign = ((org.bukkit.material.Sign) sign.getBlock().getState().getData());
        materialSign.setFacingDirection(BlockFace.SOUTH);
        sign.setData(materialSign);
        sign.setEditable(true);
        sign.setLine(0, "§6Capture the Flag");
        sign.setLine(2, "§2ONLINE");
        sign.setLine(3, "§a0§7/§a20");
        sign.update();
    }

    public void sendToServer(Player player, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch(IOException exception) {
            exception.printStackTrace();
        }
        player.sendPluginMessage(this, "BungeeCord", b.toByteArray());
    }

}

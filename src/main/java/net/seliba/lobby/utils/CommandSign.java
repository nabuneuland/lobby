package net.seliba.lobby.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public final class CommandSign implements Listener {

    public CommandSign() {
        signs = new HashMap<>();
    }

    private Map<Sign, BiConsumer<Player, Location>> signs;

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null || !(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)))
            return;

        BlockState state = event.getClickedBlock().getState();

        signs.keySet().forEach(sign -> {
            if (sign.equals(state))
                signs.get(sign).accept(event.getPlayer(), event.getClickedBlock().getLocation());
        });
    }

    private Sign create(Location location, BiConsumer<Player, Location> action, Material type) {
        location.getBlock().setType(type);

        Sign sign = (Sign) location.getBlock().getState();

        signs.put(sign, action);

        return sign;
    }

    public Sign createWallSign(Location location, BiConsumer<Player, Location> action) {
        return create(location, action, Material.WALL_SIGN);
    }
}
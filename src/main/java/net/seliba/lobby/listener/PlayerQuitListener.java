package net.seliba.lobby.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        //Remove items from Inventory
        player.getInventory().clear();

        //Change message
        event.setQuitMessage("§8[§c-§8] §7" + player.getName());
    }

}

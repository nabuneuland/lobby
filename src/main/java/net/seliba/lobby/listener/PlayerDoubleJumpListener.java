package net.seliba.lobby.listener;

import org.bukkit.*;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

import java.util.ArrayList;

public class PlayerDoubleJumpListener implements Listener {

    private ArrayList<Player> fly = new ArrayList<>();

    @EventHandler
    public void onFlyToggle(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();

        double hight = 0.3D;
        double multiply = 0.7D;

        if(player.getGameMode() != GameMode.CREATIVE && fly.contains(player.getPlayer())){
            event.setCancelled(true);
            player.setAllowFlight(false);
            player.setFlying(false);
            player.setVelocity(player.getLocation().getDirection().multiply(1.0D + multiply).setY(1.0D + hight));
            player.setFallDistance(0.0F);
            player.getLocation().getWorld().playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_FLAP, 1.0F, -5.0F);
            fly.remove(player);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location location = player.getLocation();

        if(player.getGameMode() == GameMode.CREATIVE) {
            return;
        }


        if(player.getGameMode() != GameMode.CREATIVE && location.getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR){
            player.setAllowFlight(true);
            fly.add(player);
        }

        if(location.getBlockY() > 60) {
            player.teleport(new Location(player.getWorld(), location.getX(), 46D, location.getZ(), location.getYaw(), location.getPitch()));
        }

        if(location.getBlockY() < 15) {
            player.teleport(new Location(Bukkit.getWorld("lobby"), 4.5D, 24D, -10.5D));
        }

        if(location.getBlockX() > 45) {
            player.teleport(new Location(player.getWorld(), 44D, location.getY(), location.getZ(), location.getYaw(), location.getPitch()));
        }

        if(location.getBlockX() < -38) {
            player.teleport(new Location(player.getWorld(), -37D, location.getY(), location.getZ(), location.getYaw(), location.getPitch()));
        }

        if(location.getBlockZ() > 28) {
            player.teleport(new Location(player.getWorld(), location.getX(), location.getY(), 27D, location.getYaw(), location.getPitch()));
        }

        if(location.getBlockZ() < -55.5) {
            player.teleport(new Location(player.getWorld(), location.getX(), location.getY(), -54.5D, location.getYaw(), location.getPitch()));
        }
    }

}

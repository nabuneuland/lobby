package net.seliba.lobby.listener;

import net.seliba.lobby.Lobby;
import net.seliba.lobby.utils.ItemStackBuilder;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashSet;
import java.util.Set;

public class PlayerJoinListener implements Listener {

    private final Location SPAWN_LOCATION;

    private Set<Player> hidingList = new HashSet<>();

    public PlayerJoinListener() {
        SPAWN_LOCATION = new Location(Bukkit.getWorld("lobby"), 4.5D, 24D, -10.5D);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        //Teleport player to spawn
        player.teleport(SPAWN_LOCATION);

        //Add items to Inventory
        player.getInventory().setHeldItemSlot(0);
        player.getInventory().setItem(0, new ItemStackBuilder(Material.NETHER_STAR).setName("§aNavigator").setLore("§6Teleportiere dich mit Rechtsklick!").build());
        player.getInventory().setItem(1, new ItemStackBuilder(Material.BLAZE_ROD).setName("§aSpieler verstecken").setLore("§6Verstecke andere Spieler!").build());
        player.getInventory().setItem(8, new ItemStackBuilder(Material.CHEST).setName("§aGadgets").setLore("§6Öffnet das Gadgets-Menü").build());

        //Set basic values
        player.getWorld().spawnParticle(Particle.DRAGON_BREATH, player.getLocation(), 5);
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(true);

        //Modify message
        event.setJoinMessage("§8[§a+§8] §7" + player.getName());

        //Send messages
        player.sendMessage("§aHerzlich Willkommen auf dem §6Plagindefs§8-§6Teamserver!");

        //Hide player for everyone who has this feature enabled
        hidingList.forEach(
           playerInList -> playerInList.hidePlayer(Lobby.getProvidingPlugin(Lobby.class), player)
        );
    }

    public void addToPlayerHidingList(Player player) {
        hidingList.add(player);
    }

    public void removeFromPlayerHidingList(Player player) {
        hidingList.remove(player);
    }

}

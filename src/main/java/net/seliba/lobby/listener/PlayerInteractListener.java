package net.seliba.lobby.listener;

import net.seliba.lobby.utils.ItemStackBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {

    private final int INVENTORY_SIZE = 3 * 9;

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if(event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.CHEST) {
            event.setCancelled(true);
        }

        if(event.getItem() == null) {
            return;
        }

        switch (event.getItem().getType()) {
            case NETHER_STAR:
                openNavigationInventory(player);
                player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_AMBIENT, 5f, 5f);
                break;
            case BLAZE_ROD:
                openPlayerVisibilityInventory(player);
                player.playSound(player.getLocation(), Sound.BLOCK_COMPARATOR_CLICK, 5f, 5f);
                break;
            case CHEST:
                openGadgetsInventory(player);
                player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 5f, 5f);
                break;
            default:
                break;
        }
    }

    private void openNavigationInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(null, INVENTORY_SIZE, "§aNavigator");

        //Add items to inventory
        inventory.setItem(11, new ItemStackBuilder(Material.BLUE_BANNER).setName("§6CTF").setLore("§aTeleportiere dich zu Capture the Flag").build());
        inventory.setItem(13, new ItemStackBuilder(Material.NETHER_STAR).setName("§6Spawn").setLore("§aTeleportiere dich zum Spawn").build());
        inventory.setItem(15, new ItemStackBuilder(Material.BARRIER).setName("§cBald verfügbar").build());

        fillEmptySlotsAndOpen(inventory, player);
    }

    private void openPlayerVisibilityInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(null, INVENTORY_SIZE, "§aSpieler verstecken");

        //Add items to inventory
        inventory.setItem(11, new ItemStackBuilder(Material.LIME_DYE).setName("§6Alle Spieler anzeigen").build());
        inventory.setItem(15, new ItemStackBuilder(Material.REDSTONE).setName("§6Alle Spieler verstecken").build());

        fillEmptySlotsAndOpen(inventory, player);
    }

    private void openGadgetsInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(null, INVENTORY_SIZE, "§aGadgets");

        //Add items to inventory
        inventory.setItem(13, new ItemStackBuilder(Material.BARRIER).setName("§cBald verfügbar").build());

        fillEmptySlotsAndOpen(inventory, player);
    }

    private void fillEmptySlotsAndOpen(Inventory inventory, Player player) {
        // Fill empty inventory slots with glass
        ItemStack emptySlot = new ItemStackBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").build();
        for (int i = 0; i < INVENTORY_SIZE; i++) {
            if(inventory.getItem(i) == null) {
                inventory.setItem(i, emptySlot);
            }
        }

        //Open inventory to player
        player.openInventory(inventory);
    }

}

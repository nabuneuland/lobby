package net.seliba.lobby.listener;

import net.seliba.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class InventoryClickListener implements Listener {

    private Lobby lobby;
    private final Location SPAWN_LOCATION;

    private PlayerJoinListener playerJoinListener;

    public InventoryClickListener(Lobby lobby, PlayerJoinListener playerJoinListener) {
        this.lobby = lobby;
        this.playerJoinListener = playerJoinListener;

        SPAWN_LOCATION = new Location(Bukkit.getWorld("lobby"), 4.5D, 24D, -10.5D);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getInventory();

        //Check for false Clicks
        if(event.getCurrentItem() == null) {
            return;
        }

        //Check if the inventory is the inventory of the player
        if(event.getClickedInventory() == player.getInventory()) {
            if(player.getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
            return;
        }

        switch (event.getView().getTitle()) {
            case "§aNavigator":
                event.setCancelled(true);
                handleNavigatorClick(event);
                break;
            case "§aSpieler verstecken":
                event.setCancelled(true);
                handlePlayerHiderClick(event);
                break;
            default:
                event.setCancelled(true);
                break;
        }
    }

    private void handleNavigatorClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        switch (event.getCurrentItem().getType()) {
            case BLUE_BANNER:
                player.closeInventory();
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5f, 5f);
                player.sendMessage("§6Plagindefs §7» §aDu befindest dich nun bei CTF!");
                lobby.sendToServer(player, "ctf1");
                break;
            case NETHER_STAR:
                player.closeInventory();
                player.teleport(SPAWN_LOCATION);
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5f, 5f);
                player.sendMessage("§6Plagindefs §7» §aDu befindest dich nun am Spawn!");
                break;
            default:
                break;
        }
    }

    private void handlePlayerHiderClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        switch (event.getCurrentItem().getType()) {
            case LIME_DYE:
                player.closeInventory();
                Bukkit.getOnlinePlayers()
                        .forEach(onlinePlayer ->
                                player.showPlayer(Lobby.getProvidingPlugin(Lobby.class), onlinePlayer)
                        );
                playerJoinListener.removeFromPlayerHidingList(player);
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5f, 5f);
                player.sendMessage("§6Plagindefs §7» §aDu siehst nun wieder alle Spieler!");
                break;
            case REDSTONE:
                player.closeInventory();
                Bukkit.getOnlinePlayers()
                    .forEach(onlinePlayer ->
                            player.hidePlayer(Lobby.getProvidingPlugin(Lobby.class), onlinePlayer)
                    );
                playerJoinListener.addToPlayerHidingList(player);
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5f, 5f);
                player.sendMessage("§6Plagindefs §7» §aDu hast alle Spieler versteckt!");
            default:
                break;
        }
    }

}
